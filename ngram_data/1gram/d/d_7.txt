decision M 108940699
defined M 79137577
division M 78004875
decided M 73604266
develop M 69337130
despite M 64074086
details M 54833045
divided M 54070384
defense M 45828129
depends M 45319727
demands M 43807926
desired M 43232925
discuss M 38355544
display M 37037427
degrees M 35983058
diseases M 34209008
decline M 32144109
defence M 31830544
devoted M 31462187
distant M 30223571
devices M 28958749
decrease M 28916338
depending M 28882036
dropped M 28684327
decades M 23504283
destroy M 23407845
dispute M 21494150
designs M 21436576
dignity M 20522907
desires M 19550432
delight M 17447472
divorce M 17424588
deliver M 16927493
diverse M 16872233
defects M 16165795
damages M 16092810
default M 15784707
defeated M 15550459
devotion M 14573795
declare M 14503609
despair M 14151462
delayed M 13559356
descent M 13335128
differs M 12611244
destiny M 12323081
damaged M 11296883
demanding M 11161342
deficit M 11001650
defines M 10940155
deceased M 10578029
debates M 10473173
deserve M 10017359
devised M 9882197
differing M 8996394
decreasing M 8721148
declining M 8530277
domains M 8300545
dreadful M 8194308
designing M 7943374
directing M 7694142
doubled M 7210104
doorway M 7187825
deepest M 6886880
doubted M 6835981
defending M 6796726
dislike M 6703334
decides M 6311705
denotes M 6264450
dispose M 5741274
descend M 5696971
disgust M 5507318
disturb M 5463997
decimal M 5425696
defiance M 5344710
dismiss M 5304455
drowned M 5164686
divides M 5008178
discern M 4854574
dissent M 4764987
denoted M 4572524
deprive M 4439115
drifted M 4415810
deduced M 4334623
debated M 4281473
deities M 4246479
directs M 4175108
dictate M 4070983
deceive M 4010730
diseased M 3974031
depicts M 3760156
detecting M 3690190
deserving M 3664971
delusion M 3639763
deleted M 3522786
despise M 3091390
discard M 2962533
donation M 2939623
daytime M 2921225
departing M 2773238
dormant M 2713121
deepening M 2689188
deletion M 2607870
donated M 2547558
degrading M 2478806
deposed M 2337746
disrupt M 2329094
decayed M 2120011
doubles M 2063689
distort M 2054605
disposing M 1983401
darkest M 1939574
depriving M 1817601
defends M 1812536
daybreak M 1792113
detailing M 1686411
degrade M 1608700
departs M 1485547
diverting M 1471597
devotee M 1431033
diverge M 1426265
deluded M 1424456
devouring M 1399781
deceiving M 1376343
darkening M 1358844
detract M 1324340
dataset M 1304789
disabling M 1227130
debugging M 1223775
dutiful M 1165975
diverging M 1113649
disable M 1092270
deducting M 1081473
disputing M 1026922
dictating M 894112
divulge M 694376
deepens M 681018
divisor M 674247
deferring M 556475
daunted M 549554
detaching M 535890
decoded M 521221
deletes M 470434
deterring M 451416
devises M 449840
deplete M 449225
dimness M 449128
deathless M 443805
depleting M 425294
darkens M 369967
disliking M 364564
devours M 358287
diverts M 343014
deforming M 321590
demised M 318870
deduces M 274053
decrypt M 199678
deceits M 185319
divulging M 179373
deforms M 166258
despond M 158676
doubter M 155363
donates M 147874
dismaying M 125895
deathlike M 121889
deposes M 120320
deducts M 116842
decodes M 114583
deadlier M 113252
dawdled M 101531
deludes M 77442
destine M 71966
dimmest M 70372
desisting M 55793
desists M 37216
demises M 25676
dismays M 23572
dawdles M 17484
derived D 51174016
dollars D 41646958
density D 39509749
diameter D 31717219
dynamic D 27819886
duration D 25007308
dressed D 21813667
doctors D 19521373
digital D 18517796
deposit D 16621990
deviation D 13624970
diabetes D 10657661
dynasty D 10355709
dominion D 9695091
dilemma D 9351126
dragged D 8422860
drivers D 8238027
diagrams D 7956076
declaring D 7836505
diffuse D 7610188
dietary D 7157300
derives D 6608230
duchess D 6422749
dickens D 6144263
drained D 6055609
drought D 5853893
dwellings D 5540325
dresses D 5059880
decrees D 4899609
diagonal D 4863945
durable D 4756987
diamonds D 4753657
drastic D 4701592
dancers D 4694995
drafted D 4499453
dilution D 4253983
diluted D 4205413
dementia D 4079285
diabetic D 3849849
drunken D 3580893
dialects D 3557868
deserts D 3522617
desktop D 3454060
deadline D 3406428
decreed D 3314779
drilled D 3244990
dentist D 3197698
depicting D 3197195
discord D 3071354
dilated D 2948358
disdain D 2861900
diploma D 2841529
densely D 2797058
decency D 2748803
drawers D 2746128
dinners D 2710346
dualism D 2536345
dessert D 2534035
debtors D 2503439
dialysis D 2484691
diaspora D 2266689
duality D 2243988
ditches D 2240726
deafness D 2106276
dryness D 2036166
diagnose D 1988183
diurnal D 1860060
deviance D 1827795
divines D 1755287
dolphin D 1753824
dresser D 1748923
decorum D 1698059
dragons D 1679074
demeanor D 1562494
devotes D 1531926
dressings D 1505494
drapery D 1492820
depress D 1367226
demonic D 1359616
diocesan D 1359345
derision D 1354691
deadlock D 1335448
drummer D 1323699
dauphin D 1315294
dilation D 1269721
dowager D 1266464
droplet D 1232228
diagnosing D 1214766
dazzled D 1205237
detente D 1181696
detects D 1154780
dummies D 1145824
donkeys D 1122011
debased D 1087273
dwindling D 1086992
diploid D 1079176
diffusing D 1066050
doublet D 1056929
drinker D 1053458
denture D 1044528
dosages D 1023350
dioceses D 1006080
defunct D 984663
derrick D 980408
deviated D 962290
deplore D 960596
ductile D 955686
disobey D 949209
deflect D 943008
deranged D 928302
dropout D 888257
debacle D 882563
deviates D 872506
defiled D 860127
daggers D 849728
decoder D 848818
deploying D 848335
denuded D 799893
dwarfed D 792392
diggers D 789300
dailies D 786646
defraud D 786488
dyslexia D 778800
dweller D 764907
deserting D 752135
daresay D 749073
disunion D 737083
divider D 728680
deputed D 726384
derided D 716586
drizzle D 715928
dossier D 704807
daisies D 704502
doughty D 701347
disarming D 700064
dodgers D 696478
digests D 676823
demesne D 663689
dissect D 663564
dripped D 657770
droppings D 631726
dreamers D 630565
drugged D 626376
deified D 616854
drooped D 612566
digesting D 611569
divined D 599552
despising D 594640
dredged D 587022
decried D 580762
devolve D 564696
defaced D 562075
diastole D 558557
doleful D 544306
diviner D 540265
dipoles D 539690
disused D 536255
despots D 525452
dragoon D 518171
detaining D 511748
debited D 511018
drawled D 500363
disband D 487393
dwindle D 478525
doorman D 465294
diatomic D 459041
dogwood D 457330
divorcing D 452041
dreamily D 450209
diatonic D 441048
daydream D 439660
duchies D 435665
disavow D 433165
drummed D 431774
dairies D 427802
diatribe D 426229
damsels D 424494
dervish D 421712
delimit D 416337
dumplings D 411005
diluent D 403048
dappled D 396255
dominoes D 395984
deigned D 395671
defiles D 391673
deploring D 378545
daycare D 366784
demoted D 365047
drenching D 364895
debater D 364593
devalue D 364259
dynamos D 363419
dribble D 360112
dribbling D 360075
deluged D 358507
dimples D 356098
deviants D 344857
dampers D 336632
dullest D 328483
debauch D 328123
defraying D 327256
demerit D 325680
decibel D 324523
detours D 319787
dimpled D 319202
demoniac D 318604
dilator D 316463
densest D 315972
digress D 315060
demotion D 314217
deadwood D 309473
deadened D 307219
dropper D 306128
dampening D 298580
dukedom D 296568
dignify D 290853
deictic D 290243
dilates D 285427
dockers D 281906
distill D 281122
dabbled D 278865
diathermy D 277151
ducklings D 276412
deflate D 274212
dearness D 273880
dualist D 273168
dowries D 270537
deafened D 267071
diphthong D 263683
doctoring D 261377
datable D 257983
diabolic D 256892
downers D 256646
distaff D 253355
divesting D 248884
deploys D 242647
diapason D 241664
drizzling D 241178
dugouts D 240019
drapers D 233686
devolving D 232770
drovers D 231621
deadness D 225842
declaim D 225408
debunking D 224814
dethrone D 224226
dandies D 223317
demotic D 223223
dispels D 219675
durance D 215184
drywall D 214388
dogfish D 213560
dredges D 209214
decreeing D 207704
deniers D 207229
dunghill D 206125
diastase D 205426
diesels D 204973
defamed D 204865
drippings D 204769
drifter D 203609
digraph D 201754
dutiable D 200614
denizen D 200543
ditched D 200368
direful D 199430
ditties D 199312
devaluing D 198687
despoil D 193731
deflating D 189100
detests D 181255
downbeat D 179442
demeaned D 176727
deviancy D 172622
dustbin D 171233
defused D 166524
dilutes D 165147
descant D 164747
dirtier D 163721
demigod D 162547
dobbins D 161763
duopoly D 159785
degassing D 158783
debuted D 157769
diptych D 155900
distend D 155211
dockets D 153694
douches D 152121
downswing D 150653
driller D 149310
drafter D 145686
dormers D 144757
drachma D 143147
drearily D 142582
dallied D 142084
drumbeat D 138337
dusters D 136082
deistic D 135492
dazzles D 134989
deciles D 133391
diopters D 132709
dragnet D 132073
disowning D 131681
defrost D 131243
dolmens D 131212
decoyed D 128136
dizzily D 127803
darkies D 124311
daemonic D 123550
dieters D 118630
dippers D 117944
defecting D 116017
derides D 115546
decries D 115246
doormat D 113519
deporting D 112475
desalting D 112148
debrief D 112069
dredger D 110448
doubler D 110219
disport D 108829
detains D 108505
dioramas D 106461
darkish D 102352
disarms D 100656
derringer D 100428
daddies D 99326
dethroning D 98230
doodles D 97784
decanting D 96855
dirtied D 94670
drudges D 93047
doilies D 91450
dollies D 91001
doormen D 90878
diarists D 90775
dadaism D 89244
dadaist D 89232
deluges D 89208
dowered D 88111
dazedly D 87495
deviled D 86961
debases D 86554
detesting D 85622
divests D 83100
diamante D 82858
dampens D 82595
dumbest D 81908
disrobe D 80646
devilry D 79952
doddering D 79674
drizzly D 78928
dewdrop D 78828
drownings D 78335
deputes D 78060
disrobing D 77646
deathblow D 76715
dullard D 75860
dimmers D 75782
deprave D 75194
disowns D 74563
dishpan D 74220
doggone D 73171
docketing D 69815
decider D 68952
dactyls D 68939
dithered D 68791
dustpan D 67638
drumlin D 66987
dustman D 66386
drowsed D 66310
dastard D 64394
damasks D 63947
debouch D 62562
dabbler D 61860
dickering D 61739
durably D 60092
diapered D 59963
drooled D 59575
definer D 59142
darters D 57330
derailing D 57301
deanship D 57193
drunker D 56030
dinette D 55467
debarring D 53307
decamps D 52990
demurring D 49385
dubiety D 48509
delousing D 46748
draftee D 46122
dovecot D 46037
dorsals D 45621
deadbolt D 45045
docents D 44958
debarking D 44830
debunks D 44269
drainer D 44188
dormice D 42722
dandled D 42372
dollops D 42301
doyenne D 42264
dogcart D 42088
drearier D 38212
duelist D 38182
datives D 37973
duffers D 37918
deathbeds D 37529
detouring D 37433
depraving D 37400
derails D 37174
defaces D 35998
dogsled D 35745
dabbles D 35343
dowsers D 34939
derbies D 34768
decagon D 34591
drogues D 34488
doodled D 34445
dizzied D 34019
dinginess D 33748
dishrag D 33639
dumpers D 33433
deniable D 33174
deifies D 33162
deadheads D 32832
deranges D 32101
deadbeats D 31879
dallies D 31686
deadheading D 30992
deejays D 30975
damsons D 29621
defeater D 29544
dustmen D 28656
defuses D 28067
doggies D 27453
dioxides D 27234
decamping D 26605
donnish D 26418
doltish D 25868
duennas D 25571
dungaree D 24876
descaling D 24592
dashingly D 24488
defamer D 23762
descrying D 23708
doodads D 23592
dogtrot D 23263
dratted D 22986
defames D 22544
drubbed D 22435
doweled D 22252
druggie D 21767
deathtrap D 21664
dapples D 21272
daubers D 20973
douched D 20591
dirties D 20399
dazzler D 20165
diddled D 19973
demotes D 19883
dewlaps D 19130
degrease D 18957
dotards D 18369
divvied D 18332
druthers D 18279
dustier D 18086
driveling D 18021
diddler D 17789
dashiki D 17696
drinkings D 17376
defrays D 17277
drudged D 16759
danders D 15670
deports D 15526
defiler D 15507
dibbled D 15398
dogeared D 15323
denudes D 14900
danglers D 14881
dutches D 14144
duskier D 14014
duckies D 13924
dampest D 13394
deplaning D 13098
dwarfer D 12839
deceases D 12729
droller D 12472
destining D 12463
dizzier D 12330
delayer D 12294
deplane D 12224
damningly D 12176
drowses D 11238
delvers D 10653
doglegs D 10452
dickers D 10316
dirtily D 10078
deepish D 9820
dashers D 9738
drabber D 9704
dustily D 9613
deftest D 9504
dawdler D 9473
driblet D 9439
diktats D 9234
dimwits D 9101
dinkier D 8846
delouse D 8543
deskill D 8540
darners D 8006
dejects D 7428
dodders D 7409
doddery D 7190
dreamier D 7170
dibbles D 6682
dirndls D 6587
drubbings D 6570
dickeys D 6358
daybeds D 6307
doodler D 5980
dybbuks D 5857
dewclaw D 5831
dandles D 5697
dossers D 5563
dodgems D 5535
diddles D 5487
duodena D 5456
dejecting D 5256
dubbers D 4983
dunnest D 4971
decants D 4928
dieseling D 4877
dowdies D 4797
dangered D 4796
dillies D 4671
doeskins D 4602
detoxed D 4335
dowdily D 4322
drivels D 4306
deadpans D 4283
deicers D 4274
dungeoned D 4178
degauss D 4169
dewiest D 4160
dizzies D 4071
dabbers D 4068
defrock D 4017
dumpier D 3912
deejaying D 3825
defogging D 3553
dallier D 3408
dourest D 3357
deleter D 3355
duelers D 3096
demurer D 3072
ditherer D 2923
dandier D 2705
demisting D 2559
defacer D 2532
diddums D 2403
daftest D 2402
divvies D 2389
defensing D 2372
dowdier D 2350
descale D 2340
dankest D 2340
dottier D 2294
demount D 2280
dinkies D 2229
debarks D 2214
debaser D 2159
dumpies D 2100
dumdums D 2022
disuses D 1796
disbars D 1713
dappers D 1684
damasking D 1587
detoxes D 1514
dearests D 1511
dialyzes D 1483
downier D 1352
dandering D 1169
dodgier D 995
dopiest D 900
dolloping D 765
diciest D 761
desalts D 724
dinnering D 703
degases D 642
drearies D 637
demists D 609
dazzlings D 528
deathlier D 480
demobbing D 451
doodahs D 425
deadlining D 396
daysack D 348
daffier D 348
dandify D 330
drubber D 322
dorkier D 270
doziest D 247
doggier D 219
dammits D 130
dippier D 106
