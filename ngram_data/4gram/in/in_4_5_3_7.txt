into force with respect M 9902
into acute and chronic M 9772
into which they entered M 8798
into hills and valleys M 3020
into which she entered M 2654
into thinking that because M 2460
into words and phrases M 2189
into chaos and anarchy M 2020
into which she married M 1828
into hours and minutes M 1610
into which they divided M 1602
into which they married M 1582
into order and harmony M 1460
into which our country M 1440
into which this country M 1436
into thinking that everything M 1254
into anything and everything M 1203
into civil war between M 1190
into local and general M 1118
into about one hundred M 1081
into teaching and research M 1055
into equal and similar M 1040
into which his country M 997
into which she herself M 982
into place and secured M 977
into which they develop M 974
into which you entered M 924
into which you descend M 888
into which god entered M 864
into which they project M 844
into which they finally M 844
into light and liberty M 836
into space for several M 786
into which its members M 782
into about two hundred M 776
into which both parties M 764
into which they retired M 762
into which they brought M 754
into unity and harmony M 734
into which our society M 712
into which they descend M 694
into apathy and despair M 688
into equal and unequal M 686
into facts and figures M 684
into which she dropped M 678
into touch with general M 640
into which our several M 630
into which one entered M 622
into which they carried M 620
into legal and illegal M 620
into heroes and martyrs M 610
into which that country M 606
into which his thoughts M 602
into which they stepped M 590
into basic and applied M 590
into which she stepped M 588
into which they dropped M 584
into which our thoughts M 580
into light and freedom M 580
into known and unknown M 578
into earned and unearned M 573
into exile and poverty M 564
into which they removed M 562
into doubt and despair M 544
into which this material M 534
into which all classes M 532
into which they propose M 530
into which are gathered M 528
into large and complex M 510
into which all persons M 508
into unity with himself M 502
into which they crowded M 496
into place and started M 488
into chaos and violence M 484
into which men entered M 478
into heaven not because M 476
into which our enemies M 474
into touch with certain M 456
into state and society M 452
into place and covered M 441
into signs and symbols M 438
into which his friends M 436
into which new members M 432
into heaven and becomes M 432
into jails and prisons M 422
into which they emerged M 418
into thinking that perhaps M 418
into action and started M 412
into which all members M 410
into touch with western M 408
into white and colored M 405
into which her marriage M 398
into lucky and unlucky M 392
into which god himself M 390
into which all objects M 388
into action and brought M 386
into which our friends M 384
into people and objects M 384
into which our present M 382
into which his enemies M 382
into birds and animals M 382
into which his majesty M 380
into action and results M 376
into which our subject M 372
into close and regular M 372
into exile and slavery M 368
into which are crowded M 366
into place and stepped M 364
into words and numbers M 360
into place and install M 360
into paper and printed M 360
into thinking that certain M 358
into which all parties M 356
into valid and invalid M 356
into force for parties M 356
into grief and despair M 350
into which they emptied M 342
into unions and bargain M 341
into which this subject M 332
into which she appeared M 330
into moral and immoral M 330
into longer and shorter M 330
into which are brought M 328
into which his destiny M 324
into touch with several M 324
into which this chapter M 322
into which all returns M 318
into decay and oblivion M 317
into decay and neglect M 317
into which his ambition M 316
into exile and reduced M 316
into close and confiding M 316
into which this history M 314
into which his affairs M 312
into within and between M 310
into nearly all aspects M 304
into action and produce M 304
into moral and natural M 302
into which they usually M 300
into which they lowered M 300
into which his parents M 300
into nearly two hundred M 300
into which his brothers M 298
into power and control M 296
into action and quickly M 296
into thinking that somehow M 294
into which they gathered M 292
into state and private M 290
into healthy and unhealthy M 288
into exile for thirteen M 288
into which his weakness M 284
into fixed and current M 284
into which one happens M 282
into cells that produce M 280
into crime and violence M 276
into error with respect M 274
into which they combine M 272
into error and delusion M 269
into words and symbols M 262
into water and allowed M 262
into nearly one hundred M 262
into every new passage M 254
into civil war against M 254
into which was crowded M 252
into quiet and silence M 250
into which this element M 248
into which our species M 248
into place and allowed M 248
into space and wondering M 246
into touch with persons M 244
into which they convert M 240
into which all symbols M 240
into which she retired M 236
into chaos and despair M 236
into danger and trouble M 234
into which one hundred M 232
into decay and poverty M 232
into which his passions M 230
into which they withdraw M 228
into which they quickly M 228
into which they climbed M 228
into decay and finally M 226
into exile and brought M 224
into words for himself M 222
into which she desired M 222
into exile for several M 222
into labor and capital M 221
into which two factions M 218
into action and carried M 218
into which they believe M 216
into empty air returning M 216
into fixed and movable M 210
into which was gathered M 208
into place and pressed M 208
into action and provide M 208
into caves and tunnels M 207
into touch with members M 206
into topic and comment M 206
into thinking for himself M 206
into space and finally M 206
into space and becomes M 206
into which his religion M 204
into cells that contain M 204
into action and because M 203
into which new material M 200
into pairs and discuss M 200
into lines and columns M 200
into which this question M 198
into which she gathered M 196
into which they withdrew M 194
into which ten thousand M 194
into which his marriage M 194
into place and becomes M 194
into labor and deliver M 194
into drive and started M 194
into which this problem M 192
into which she brought M 192
into which they attempt M 190
into touch and sympathy M 190
into really big trouble M 190
into lines and circles M 190
into close and familiar M 190
into civil war because M 190
into which they drifted M 188
into warmth and comfort M 188
into first and started M 188
into which all natural M 186
into nearly two thousand M 186
into which men spoiled M 184
into types and classes M 182
into small and hostile M 182
into words and letters M 180
into which his company M 180
into thinking and believing M 180
into space and returning M 180
into shape for herself M 180
into lengths not exceeding M 180
into force each trembling M 180
into place and everything M 179
into which they desired M 178
into which are dropped M 176
into space and brought M 176
into place and hurried M 176
into local and central M 176
into glory and freedom M 176
into action that results M 176
into which had entered M 174
into which this passage M 172
into which his peculiar M 172
into water and drowned M 172
into place and removed M 172
into place and finally M 172
into which this finally M 170
into which are entered M 170
into space and towards M 170
into motion and advancing M 170
into writing and producing M 168
into which they invited M 168
into which they appeared M 166
into trees and animals M 166
into shape for service M 166
into hills and forests M 164
into action too quickly M 164
into under this chapter M 162
into nearly its present M 162
into loves and desires M 162
into anything too quickly M 162
into worse than slavery M 160
into which two hundred M 160
into which his subject M 160
into which each society M 160
into boxes and carried M 160
into which they inquire M 158
into which its history M 158
into space and dropped M 158
into human and natural M 158
into really bad trouble M 156
into heaven was adopted M 156
into action and becomes M 156
into which they exactly M 154
into which all animals M 154
into drive and stepped M 154
into close and helpful M 154
into action and finally M 154
into words and thoughts M 152
into thinking that religion M 152
into place and checked M 152
into parts and examine M 152
into guilt and despair M 152
into action and develop M 152
into thinking that language M 151
into space and bounded M 150
into about two thousand M 150
into which they evolved M 148
into which she finally M 148
into which his natural M 148
into which are divided M 148
into panic and despair M 148
into money and applied M 148
into words his thoughts M 146
into which two persons M 146
into which her thoughts M 146
into touch with friends M 146
into focus and exactly M 146
into study and research M 144
into motion and started M 144
into which her parents M 142
into which are grouped M 142
into leaves not exceeding M 142
into clothing and shelter M 142
into which you stepped M 140
into which this species M 140
into which they allowed M 140
into which one stepped M 140
into which her country M 140
into which has entered M 140
into rough and unequal M 140
into place and dropped M 140
into folly and madness M 140
into beauty and harmony M 140
into space and returns M 139
into which you propose M 138
into which they pressed M 138
into drive and pressed M 138
into which man entered M 136
into order they divided M 136
into which its capital M 134
into speaking out against M 134
into place with respect M 134
into money and capital M 134
into which all present M 133
into which each country M 132
into which all aspects M 132
into touch with current M 132
into space and perhaps M 132
into running for congress M 132
into place and glanced M 132
into which men descend M 130
into which his talents M 130
into forms and colours M 129
into which she escapes M 128
into exile and oblivion M 128
into chaos and turmoil M 128
into which any content M 126
into writing and directing M 124
into which they operate M 124
into thinking that service M 124
into parts and examining M 124
into crime and suicide M 124
into which any uniform M 123
into labor and material M 123
into which they grouped M 122
into which its affairs M 122
into touch with everything M 122
into place and quickly M 122
into money and carried M 122
into joint and several M 122
into exile and finally M 122
into action with respect M 122
into about six hundred M 121
into words for herself M 120
into words and finally M 120
into which she invited M 120
into sending and receiving M 120
into parts and discuss M 120
into sleep and silence M 119
into action and monitor M 119
into which this society M 118
into which she drifted M 118
into which she allowed M 118
into which our blessed M 118
into which god invites M 118
into which are pressed M 118
into walls and windows M 118
into thinking that freedom M 118
into space for minutes M 118
into place and lowered M 118
into anything that offered M 118
into action and ordered M 118
into civil and natural M 117
into which you dropped M 116
into which this ancient M 116
into which she hastily M 116
into which our animals M 116
into which his measures M 116
into which his letters M 116
into water and observing M 116
into place and stopped M 116
into labor and trouble M 116
into which each subject M 115
into woods and forests M 114
into which that material M 114
into which she climbed M 114
into which his windows M 114
into people and animals M 114
into heaven and eternal M 114
into force was delayed M 114
into daily and familiar M 114
into which that complex M 113
into which this process M 112
into which she planned M 112
into shape and service M 112
into place and climbed M 112
into apathy and neglect M 112
into action and destroy M 112
into which you analyze M 110
into which they receive M 110
into which they planned M 110
into which they conduct M 110
into which our culture M 110
into which all letters M 110
into vague and general M 110
into touch with similar M 110
into touch with distant M 110
into civil war history M 110
into cakes and wrapped M 110
into apathy and inaction M 110
into abuse and neglect M 110
into action with renewed M 109
into wider use because M 108
into which they escaped M 108
into which they deliver M 108
into which his fearless M 108
into moral and aesthetic M 108
into light and believe M 108
into folds and covered M 108
into earthly and heavenly M 108
into anything that happens M 108
into about one thousand M 108
into moral and material M 107
into which was pressed M 106
into which this unhappy M 106
into which she emptied M 106
into which one dropped M 106
into walls and buildings M 106
into thinking that history M 106
into power and prevent M 106
into exile who founded M 106
into beauty and delight M 106
into action and applied M 106
into which they collect M 104
into which one divides M 104
into which her friends M 104
into units and lessons M 104
into speaking and listening M 104
into space and explore M 104
into exile and founded M 104
into error and illusion M 104
into shape and position M 103
into which she divided M 102
into which his position M 102
into which all society M 102
into touch with teachers M 102
into close and repeated M 102
into books and records M 102
into action and grabbed M 102
into which this present M 101
into which one retires M 100
into walks and gardens M 100
into power and carried M 100
into people who believe M 100
into happy and unhappy M 100
into goods that satisfy M 100
into action and prevent M 100
into which this project M 98
into which they refused M 98
into which she withdrew M 98
into which his sisters M 98
into which goes everything M 98
into which all mortals M 98
into touch with affairs M 98
into stuff that spirits M 98
into realms far removed M 98
into power and started M 98
into place and brought M 98
into parts and explain M 98
into pairs and provide M 98
into others for himself M 98
into force and becomes M 98
into focus with respect M 98
into focus and provide M 98
into focus and becomes M 98
into books may connect M 98
into which this concept M 96
into which they arrived M 96
into which all entered M 96
into thinking that victory M 96
into grief and anxiety M 96
into action and arranged M 96
into doubt and dispute M 95
into which our history M 94
into which her affairs M 94
into trees and buildings M 94
into touch with society M 94
into touch with objects M 94
into hills that appeared M 94
into grace and harmony M 94
into action with perfect M 94
into folds that project M 93
into which was brought M 92
into which this general M 92
into which they channel M 92
into which our language M 92
into which his anxiety M 92
into which are emptied M 92
into space and silence M 92
into shape and members M 92
into place with several M 92
into people and society M 92
into keeping her opinions M 92
into every new project M 92
into action and managed M 92
into action and deliver M 92
into which two parties M 90
into which this capital M 90
into which this brought M 90
into which they suppose M 90
into which she invites M 90
into warmth and honesty M 90
into touch with natural M 90
into space and listening M 90
into shape and started M 90
into forms and figures M 90
into defeat and despair M 90
into chaos and brought M 90
into cakes for general M 90
into action for several M 90
into action for himself M 90
into shame and silence M 89
into lines and colours M 89
into which that capital M 88
into which his remains M 88
into which his conduct M 88
into which each student M 88
into touch with captain M 88
into thinking that success M 88
into thinking that marriage M 88
into seeking for treasure M 88
into power and brought M 88
into place with perfect M 88
into place and arranged M 88
into pairs and parties M 88
into motion with respect M 88
into months and finally M 88
into exile and silence M 88
into ethics and religion M 88
into action with increasing M 88
into action and decided M 88
into which her majesty M 87
into which this immense M 86
into which our natural M 86
into which his fellows M 86
into which had drifted M 86
into weaker and stronger M 86
into touch with admiral M 86
into shape and allowed M 86
into money and divided M 86
into magic and religion M 86
into killing her because M 86
into hills and descend M 86
into forms and ciphers M 86
into ashes and oblivion M 86
into anything that touches M 86
into action with lightning M 86
into healthy and diseased M 85
into which you brought M 84
into which was entered M 84
into which this position M 84
into which this company M 84
into which she claimed M 84
into which our efforts M 84
into which his success M 84
into which god brought M 84
into which all workers M 84
into water was drowned M 84
into units that reflect M 84
into place and replace M 84
into nearly one thousand M 84
into meeting his demands M 84
into keeping his promise M 84
into heaven and descend M 84
into fixed and certain M 84
into words and figures M 83
into action and attempt M 83
into words and language M 82
into which she managed M 82
into which its victims M 82
into which its surface M 82
into which his emotions M 82
into weather and climate M 82
into water for several M 82
into touch with sources M 82
into shame and remorse M 82
into place and watched M 82
into place and crossed M 82
into night and silence M 82
into local and private M 82
into lands far removed M 82
into exile for defending M 82
into every new country M 82
into evening and weekend M 82
into words and explain M 80
into which two classes M 80
into which this research M 80
into which that history M 80
into which she carried M 80
into which his private M 80
into shape for traveling M 80
into power and decided M 80
into hunger and poverty M 80
into every ten minutes M 80
into breaking his promise M 80
into about ten thousand M 80
into force his majesty M 56
into place and position M 55
into action and conduct M 46
into decay and decline M 45
into which that subject M 44
into author and subject M 43
into action and display M 43
into chaos and tyranny M 42
into keeping him company M 41
into which this content M 40
into signs and wonders M 40
into action and control M 40
into medial and lateral D 11578
into fixed and variable D 10674
into males and females D 5754
into holes and corners D 3946
into state and national D 2039
into local and regional D 1798
into state and federal D 1688
into calyx and corolla D 1658
into wages and profits D 1622
into which her husband D 1610
into bowls and garnish D 1603
into local and national D 1535
into sects and parties D 1526
into nooks and corners D 1486
into drugs and alcohol D 1416
into homes and offices D 1382
into thorax and abdomen D 1353
into cells and tissues D 1178
into genus and species D 1146
into leaves and flowers D 1096
into motor and sensory D 1080
into rafts and floated D 1052
into court and testify D 794
into molds and allowed D 762
into which she marries D 744
into noble and ignoble D 676
into shops and offices D 672
into sects and schools D 654
into lines and stanzas D 633
into which they intrude D 612
into stars and planets D 610
into water and nitrous D 603
into sales and marketing D 600
inch holes are drilled D 582
into lords and commons D 580
into yarns and fabrics D 573
into china and southeast D 557
into court and charged D 542
into lakes and marshes D 528
into trees and flowers D 520
into gloom and despair D 504
into hours and fretted D 502
into which are screwed D 500
into doors and windows D 486
into homes and schools D 460
into which they crawled D 452
into median and lateral D 451
into parks and gardens D 442
into place and tighten D 434
into which raw produce D 430
into sects and factions D 430
into which she slipped D 426
into water too shallow D 414
into touch with foreign D 410
into which they deposit D 409
into which they migrate D 404
into ranks and classes D 398
into parts and sections D 394
into ranks and marched D 392
into farms and ranches D 391
into force and duration D 389
into overt and express D 381
into court and ordered D 380
into which all mankind D 368
into balls and flatten D 358
inns where they stopped D 358
into court and country D 356
into fists and pressed D 346
into which they slipped D 340
into asses and animals D 336
into which she ushered D 324
into which they resolve D 318
into sight and stopped D 312
into which they settled D 310
into sport and society D 310
into solving this problem D 308
into holes and ditches D 304
into gases and liquids D 304
into pools and ditches D 301
into birds and mammals D 300
into local and foreign D 297
into which they secrete D 294
into wages and surplus D 289
into cells that express D 288
into which one inserts D 286
into clans and lineages D 284
into urban and regional D 280
into sacks and carried D 278
into court and accused D 278
into farms and gardens D 272
into flame and crashes D 266
into place and smoothed D 264
into rival and hostile D 256
into court and explain D 252
into killing her husband D 248
into sales and profits D 247
into which our national D 246
into knees for vessels D 246
into barns and stables D 245
into banking and finance D 243
into gowns and bodices D 242
into court and murders D 242
into grass and flowers D 240
into races and factions D 236
into carts and carried D 234
into drive and floored D 232
into which his beloved D 230
into trade and finance D 223
into roots and affixes D 222
into which all revenue D 218
into strong and durable D 218
into sands and gravels D 218
into words that express D 216
into which they stuffed D 216
into lawns and gardens D 216
into noble and opulent D 214
into homes and buildings D 214
into exile and settled D 214
into holes and gullies D 210
into fists and pounded D 210
into bones and muscles D 210
into which she inserts D 208
into singlet and triplet D 208
into place and sutured D 208
into sines and cosines D 207
into earnings and profits D 202
into canoes and paddled D 202
into steaks and broiled D 200
into green and fertile D 200
into sects and schisms D 198
into which they marched D 194
into which his servant D 194
into stems and affixes D 194
into paste and applied D 194
inch below its surface D 194
into which are crammed D 192
into wards for election D 192
into touch with colonel D 190
into sugar and alcohol D 188
into small and smaller D 188
into worthy and unworthy D 186
into which you deposit D 186
into court and present D 186
into blood and tissues D 186
into which his features D 184
into space with sputnik D 184
into racial and national D 184
into boxes and barrels D 184
into atoms and numbers D 184
into which they dragged D 182
into poetry and romance D 182
into cider and vinegar D 182
into which are plugged D 181
into piles and covered D 180
into which his youthful D 178
into fruit and flowers D 176
into flats and offices D 176
into every new formula D 176
into which they tumbled D 174
into brisk and mirthful D 174
into ropes and cordage D 172
into action and shelled D 172
into focal and diffuse D 171
into wells than potatoes D 170
into hills and hollows D 170
into woods and deserts D 168
into elite and popular D 168
into which she stuffed D 166
into state and regional D 166
into sales and service D 166
into flour and shipped D 166
into flame and consume D 166
into court and secured D 166
into caves and deserts D 166
into which his previous D 164
into sharp and painful D 164
into fable and romance D 164
into which she bundled D 162
into boxes and shipped D 162
into romps and hoydens D 160
into vogue and remains D 158
into thinking that science D 158
into bales and shipped D 158
into water and stirred D 156
into which they infused D 154
into telling him everything D 154
into frogs and lizards D 154
into court and declare D 154
into cakes for storage D 154
inch above its surface D 154
into scorn and derision D 152
into parts and assigning D 152
into lines and furrows D 152
into court for failure D 152
into boats and carried D 152
into ports and harbors D 150
into others yet smaller D 150
into balls and wrapped D 150
into which they steered D 148
into drive and stomped D 148
into shape and riveted D 147
into which this rapture D 146
into space for millions D 146
into fewer and stronger D 146
into which his cousins D 144
into which any foreign D 144
into rills and gullies D 144
into which his brethren D 142
into which are wrought D 142
into ponds and marshes D 142
into maple leaf gardens D 142
into which was twisted D 140
into which one marries D 140
into place and snapped D 140
into order and decency D 140
into cells that secrete D 140
into which this science D 138
into libel and slander D 138
into grave and weighty D 138
into court for violation D 138
into armed and unarmed D 138
into hills and ravines D 137
into steady and unsteady D 136
into rhythm and harmony D 136
into meadow and pasture D 136
into boxes and baskets D 136
into which was stuffed D 134
into which she dragged D 134
into sacks and shipped D 134
into pikes and helmets D 134
into kinds and species D 132
into court and decided D 132
into changing its position D 132
into basic and derived D 132
into which they crammed D 130
into which raw material D 130
into place and settled D 130
into court for decision D 130
into caves and caverns D 130
into black and ghastly D 130
into words and express D 129
into holes and caverns D 129
into which she crushed D 128
into quiet and peaceable D 128
into holes and hollows D 127
into dutch and printed D 127
into vogue that refined D 126
into place and screwed D 126
into place and slipped D 124
into mooning and imagining D 124
into kinds and grouped D 124
into kinds and classes D 122
into balls and dropped D 122
into social and natural D 121
into which they excrete D 120
into which she retreats D 120
into which she infused D 120
into touch with liberal D 120
into paper and stamped D 120
into flame and crashed D 120
into bloom with audible D 120
into voice and gesture D 118
into farming for himself D 118
into which was screwed D 116
into order and discord D 116
into magma and install D 116
into lines and puckers D 116
into changing his position D 116
into which this poetical D 114
into water and nascent D 114
into races and classes D 114
into lipid and protein D 114
into ethnic and national D 114
into broad and shallow D 114
into which she stirred D 112
into which all previous D 112
into suits and dresses D 112
into human sex behavior D 112
into bands and sections D 112
into which they subside D 110
into which her chamber D 110
into slums and ghettos D 110
into muffs and tippets D 110
into court and offered D 110
into which this treatise D 108
into which our foreign D 108
into stars and systems D 108
into fixed and erratic D 108
into changing his behavior D 108
into blocs and spheres D 108
into action and behavior D 108
into caves and grottoes D 107
into shops and dwellings D 106
into search and inquiry D 106
into ponds and ditches D 106
into frank and faithful D 106
into zones and sectors D 104
into which was crammed D 104
into which her cousins D 104
into treaty and alliance D 104
into sugar and carried D 104
into space with rockets D 104
into shops and private D 104
into shape with hammers D 104
into sects and classes D 104
into lobes and saddles D 104
into which they crashed D 102
into smoke and cinders D 102
into logic and physics D 102
into which they retract D 100
into which they diffuse D 100
into loyal and devoted D 100
into legal and medical D 100
into bound and unbound D 100
into worms and dragons D 98
into which was wrought D 98
into which they venture D 98
into strings for musical D 98
into pearls and diamonds D 98
into gloom and sadness D 98
into finer and coarser D 98
into fewer and simpler D 98
into fetus and amniotic D 98
into edges and furrows D 98
into drugs and violence D 98
into ashes and cinders D 98
into social and welfare D 97
into chest and abdomen D 97
into which they drained D 96
into which she crawled D 96
into which are mounted D 96
into towns and suburbs D 96
into pools and plashes D 96
into pearls and precious D 96
into folds and furrows D 96
into files and folders D 96
into court for benefit D 96
into basic lead acetate D 96
into which his official D 94
into which god infuses D 94
into which all national D 94
into treaty with several D 94
into social and national D 94
into loose and lawless D 94
into court for judicial D 94
into bowls and drizzle D 94
into blues and purples D 94
into touch with popular D 92
into proud and bloated D 92
into kinds and degrees D 92
into court and certify D 92
into changing his decision D 92
into caves and hollows D 92
into yards and gardens D 90
into which that mineral D 90
into which her bedroom D 90
into unity and concord D 90
into stars and sidereal D 90
into sound and unsound D 90
into right and oblique D 90
into moist and layered D 90
into green and seasoned D 90
into court who desired D 90
into changing its behavior D 90
into which that pontiff D 88
into which his kinsman D 88
into talks and discuss D 88
into smoking and silence D 88
into sheds and cellars D 88
into roads and bridges D 88
into rhythm they vibrate D 88
into place and fastening D 88
into homes and streets D 88
into which one injects D 87
into verse and hitches D 87
into which she settled D 86
into touch with national D 86
into thorns and thistles D 86
into tanks and allowed D 86
into study and inquiry D 86
into spray and blinded D 86
into skips and hoisted D 86
into plate and jewelry D 86
into boxes for storage D 86
into farms and estates D 85
into error and impiety D 85
inch every ten minutes D 85
into waves and ripples D 84
into sight and general D 84
into sight and dropped D 84
into sacks and drowned D 84
into parks and pleasure D 84
into linen and cordage D 84
into labor her husband D 84
into forts and castles D 84
into dreams and fancies D 84
into court and attempt D 84
into which our science D 82
into which his thwarted D 82
into which each village D 82
into towns and hamlets D 82
into space and mumbled D 82
into place and clamped D 82
into pills with extract D 82
into opening its markets D 82
into helping her husband D 82
into farms and hamlets D 82
into ethnic and regional D 82
into cells for storage D 82
into which this popular D 80
into which they wrought D 80
into sacks and dropped D 80
into gloom and silence D 80
into flame and destroy D 80
into boxes and cartons D 80
into local and express D 67
into which are slotted D 56
inch balls and flatten D 46
into penis and scrotum D 44
into daily and nightly D 44
into sleep and slumber D 42
into which are stuffed D 40
into smooth and striated D 40
