make an active contribution M 3814
make the proper adjustments M 3536
make an annual contribution M 3500
made no direct contribution M 1816
make the needed adjustments M 1814
make no direct contribution M 1502
made by various individuals M 1492
made an enduring contribution M 1370
making an active contribution M 1198
made an active contribution M 1018
make an initial contribution M 1002
made the greater contribution M 946
making the proper adjustments M 906
making is highly centralized M 886
made an excess distribution M 852
made no further contribution M 842
made an annual contribution M 832
made an initial contribution M 774
made by various governments M 754
make the church independent M 749
made of various combinations M 700
make no further contribution M 686
making an annual contribution M 670
make no greater contribution M 612
make the greater contribution M 608
made the judges independent M 594
make an enduring contribution M 578
male or female individuals M 544
make no useful contribution M 510
made to ensure consistency M 504
make as little disturbance M 500
making the attack demonstrate M 466
making it through adolescence M 446
making as little disturbance M 432
made by former governments M 432
made the proper adjustments M 410
made an excess contribution M 382
make it almost unnecessary M 378
made no further observations M 378
mass is evenly distributed M 375
making the needed adjustments M 372
made no special contribution M 356
made the formal introduction M 349
made so little improvement M 344
make the initial appointment M 334
make no further observations M 330
made by various institutions M 322
male or female development M 315
make my humble contribution M 312
make it through adolescence M 310
made no further discoveries M 304
make the larger contribution M 300
made the larger contribution M 296
made to enable individuals M 294
making the church independent M 293
made my modest contribution M 284
making the simple complicated M 280
make the proper observations M 270
make the proper distribution M 268
made by public institutions M 260
make my little contribution M 258
made to obtain satisfaction M 256
made to highly compensated M 256
making the judges independent M 254
made it almost unnecessary M 254
make an obvious contribution M 252
making the measure permanently M 250
made the initial contribution M 250
male or female personality M 248
make the judges independent M 248
making an enduring contribution M 240
make us unique individuals M 240
make the further development M 240
make an actual contribution M 240
made in various combinations M 238
making no direct contribution M 236
make the system independent M 236
make the family independent M 236
make an annual distribution M 236
made no serious contribution M 232
made an urgent appointment M 230
make an initial appointment M 228
made by member governments M 226
made it through adolescence M 222
make it without interruption M 220
made the initial observations M 220
made the highly significant M 216
made no little contribution M 216
made by highly compensated M 216
make it behave differently M 214
made the further development M 214
making is further complicated M 213
many as twenty individuals M 212
making the initial appointment M 212
make the annual contribution M 212
male or female descendants M 210
made to public institutions M 208
made an almost simultaneous M 208
made by various combinations M 206
making the actual measurements M 204
make the proper applications M 202
making the proper calculations M 200
make the proper calculations M 200
make no further commitments M 198
made no useful contribution M 198
make the creation conceivable M 196
make the mental adjustments M 194
make the further substitution M 192
make an urgent appointment M 192
made to various individuals M 190
made in better understanding M 190
mark of mutual satisfaction M 188
make the actual measurements M 188
made no special appointment M 188
make the system transparent M 186
made of existing institutions M 186
made the initial discoveries M 184
make the system sustainable M 183
make the actual distribution M 182
made no direct observations M 182
made to reduce unnecessary M 180
making the person unavailable M 178
making the greater contribution M 178
making an initial contribution M 178
mark is highly distinctive M 176
make us almost universally M 176
made the crucial contribution M 176
made to behave differently M 175
made to select individuals M 174
made in energy conservation M 174
making the proper improvement M 172
make the widest conjectures M 170
make the future predictable M 170
make the changes recommended M 170
made the initial appointment M 170
make the second alternative M 169
make the result independent M 166
mass of matter accumulated M 164
made by divine appointment M 164
make the utmost improvement M 162
make the utmost contribution M 162
made no greater contribution M 162
make it wholly unnecessary M 160
make an actual distribution M 160
making the proper applications M 156
made no formal commitments M 156
made it almost unavoidable M 155
making the various adjustments M 154
making the proper substitution M 154
make my modest contribution M 154
made the church independent M 154
made an obvious contribution M 152
make the proper comparisons M 150
make the initial introduction M 150
mass of detail accumulated M 148
making the actual calculations M 148
make the actual calculations M 148
made to various institutions M 148
make the unique contribution M 144
make the proper substitution M 144
make the proper measurements M 144
making the proper distribution M 142
make the various calculations M 142
make no further disturbance M 142
make no serious contribution M 140
made no better improvement M 139
mark of public disapproval M 138
making the larger contribution M 138
make it highly advantageous M 138
made in mutual understanding M 138
made by credit institutions M 138
make the various adjustments M 136
make the slight adjustments M 136
make the initial distribution M 136
make the entire contribution M 136
make the actual appointment M 136
make no further improvement M 136
made to obtain approximate M 136
made to member governments M 136
made to measure differences M 136
made by various professional M 136
make the latter alternative M 135
making the initial adjustments M 134
making the entire performance M 132
making an excess contribution M 132
make an honest contribution M 132
make an already complicated M 131
made to obtain confirmation M 130
made to adjust differences M 130
make the proper explanations M 128
make the future development M 128
made to impose restrictions M 128
made to better accommodate M 128
made the formal appointment M 128
made so little contribution M 128
made it wholly unnecessary M 128
make the latter unnecessary M 126
make no special contribution M 126
made the needed adjustments M 126
male or female adolescents M 125
made no further significant M 125
made the proper explanations M 124
made no slight contribution M 124
mark the points corresponding M 122
many an anxious consultation M 122
make the system inefficient M 122
make the system ineffective M 122
make no further adjustments M 122
made an unique contribution M 122
made an answer unnecessary M 122
made an annual distribution M 122
mark the further development M 120
making the further development M 120
make the system competitive M 120
make it wholly independent M 120
made the latter alternative M 120
made it almost perceptible M 120
mass of papers accumulated M 118
make to remain competitive M 118
make the market competitive M 118
make the existing institutions M 118
making an initial appointment M 116
make the proper consistency M 116
make the needed measurements M 116
make the initial contribution M 116
make the actual observations M 116
made to measure personality M 116
making the proper measurements M 114
make the wisest indifferent M 114
make the normal distribution M 114
make an object transparent M 114
made in common conversation M 114
mass or volume distribution M 112
mass of furious individuals M 112
making the utmost contribution M 112
make the system accountable M 112
make the needed observations M 112
made to detect differences M 112
made no public proclamation M 112
made by direct measurements M 112
make the simple complicated M 111
making the actual observations M 110
make the simple calculations M 110
make it highly competitive M 110
made to secure distribution M 110
made the common denominator M 110
made no further independent M 110
make the subtle adjustments M 108
make it almost unavoidable M 108
made to measure performance M 108
made by direct observations M 108
making no useful contribution M 106
make up another significant M 106
make the system unavailable M 106
make no further explanations M 106
made to reduce uncertainty M 106
made the initial introduction M 106
made in direct consultation M 106
made by various instruments M 106
many an earnest conversation M 105
many as twelve individuals M 104
made no unique contribution M 104
made in existing institutions M 104
made at points corresponding M 104
make the sampling distribution M 102
make the minute adjustments M 102
made to figure prominently M 102
made no special observations M 102
made it almost universally M 102
made in import substitution M 102
make the needed calculations M 100
make the actual performance M 100
made to suffer unnecessary M 100
made to secure consistency M 100
made to obtain measurements M 100
made to effect improvement M 100
mark the future development M 98
make the causal relationship M 98
make the animal conspicuous M 98
make no obvious contribution M 98
made to obtain independent M 98
made no further commitments M 98
made it highly competitive M 98
made by normal individuals M 98
made an actual contribution M 98
make the needed improvement M 96
make the needed comparisons M 96
make the formal appointment M 96
make an unique contribution M 96
make an obvious improvement M 96
made on various unprotected M 96
made no further disturbance M 96
made by direct substitution M 96
mass of useful observations M 94
many an earlier adventurous M 94
making an actual contribution M 94
make the object transparent M 94
make the initial adjustments M 94
make the entire performance M 94
made the common inheritance M 94
made in public institutions M 94
made an unfair distribution M 94
male or female professional M 93
made by another professional M 93
male to female distribution M 92
make the truest observations M 92
make no better contribution M 92
made to secure independent M 92
made to obtain compensation M 92
made to expand construction M 92
making no further contribution M 90
made to relate differences M 90
mark of higher development M 88
making the future inheritance M 88
made the various inhabitants M 88
made no active contribution M 88
made my humble contribution M 88
made it almost independent M 88
made by another independent M 88
mass of liquid transported M 86
making the various calculations M 86
making the latter unavailable M 86
made to relate personality M 86
made no marked improvement M 86
made in actual construction M 86
made an initial appointment M 86
male to female relationship M 84
making the lights transparent M 84
make the various combinations M 84
made to secure alternative M 84
made to remain permanently M 84
made by visual observations M 84
making the proper appointment M 82
making the actual construction M 82
make the formal introduction M 82
made to assist individuals M 82
made the future development M 82
made my little contribution M 82
making the system independent M 80
making the initial observations M 80
make the proper introduction M 80
make the entire distribution M 80
make the entire development M 80
made to secure improvement M 80
made an earlier contribution M 80
made the second alternative M 56
made an already complicated M 51
make the latter independent M 48
made up almost exclusively D 11142
make the proper arrangements D 3054
mark the second anniversary D 1396
make the proper distinctions D 1362
male to female transsexual D 1358
male to female transmission D 1311
making of global citizenship D 1199
make an annual appropriation D 1138
made an annual appropriation D 1106
mark is merely descriptive D 1097
make the travel arrangements D 1096
made in various departments D 986
made the proper arrangements D 978
make the school environment D 904
making of mission communities D 799
made the travel arrangements D 790
made the proper dispositions D 778
make the client comfortable D 776
make the proper preparations D 744
made it almost impregnable D 720
make the income distribution D 652
male or female confederate D 644
making the proper arrangements D 640
make the proper dispositions D 628
make the garden unwholesome D 616
made the formal presentation D 600
made no special preparations D 598
making an annual appropriation D 590
make or unmake governments D 580
making or managing investments D 570
made no special arrangements D 552
make the person comfortable D 528
making of female masculinity D 520
male or female prostitutes D 516
make the formal presentation D 508
made in freely convertible D 500
made the proper preparations D 486
made by direct microscopic D 486
made an initial appropriation D 476
making the school environment D 444
making the travel arrangements D 422
make the offering speculative D 414
make the latter responsible D 410
made the initial arrangements D 410
make the actual presentation D 396
made no serious preparations D 390
make the victim comfortable D 378
making the proper distinctions D 374
made no further experiments D 374
make the guests comfortable D 368
made to depend exclusively D 362
make the future predominate D 353
made by various departments D 348
make the actual solicitation D 340
mass of peasant proprietors D 338
make the system determinate D 336
making the actual solicitation D 330
make the needed investments D 330
make the entire organization D 330
made by public enterprises D 326
making the latter responsible D 318
make the master responsible D 316
made by direct recruitment D 316
make it readily identifiable D 314
making the empire independent D 312
male or female infertility D 308
making the proper dispositions D 308
made it almost intolerable D 308
make the initial disclosures D 302
made in public expenditure D 302
make the family comfortable D 296
make the subtle distinctions D 292
mark the seventh anniversary D 290
made an active participant D 290
make the church subservient D 286
make the various employments D 284
make the initial presentation D 284
male or female homosexuals D 283
made by public corporations D 282
making the income distribution D 278
make no special arrangements D 276
made in casual conversation D 276
make the police accountable D 267
male or female progenitors D 266
making the proper preparations D 262
make my family comfortable D 260
make the initial investments D 256
make it almost impregnable D 254
made to secure legislative D 252
made in animal experiments D 252
make the colony independent D 250
make the system insensitive D 248
make the native commodities D 246
made to reduce expenditure D 245
made to settle differences D 244
made in another jurisdiction D 244
made my little contrivance D 242
made no violent exclamations D 240
maze of narrow passageways D 238
male or female circumcision D 234
make it highly implausible D 232
make it almost prohibitive D 232
make the needed arrangements D 230
made the church subservient D 230
made in manner hereinafter D 229
made by another participant D 228
mark of modern civilization D 224
male is readily distinguished D 224
make the voyage comfortable D 224
make the initial arrangements D 224
making the various arrangements D 220
making it highly susceptible D 218
make the island independent D 218
made at various frequencies D 218
make an initial presentation D 214
made by public accountants D 214
making of special arrangements D 212
made my travel arrangements D 212
make the troops comfortable D 208
make it highly susceptible D 208
make the flight arrangements D 206
make the corpse presentable D 204
make no further investments D 204
make an entire consecration D 202
make the needed preparations D 200
made it highly susceptible D 200
made by larger communities D 196
make an annual subscription D 194
making the initial arrangements D 190
make the rhymes perceptible D 190
make it highly questionable D 186
make the person responsible D 184
making of stringed instruments D 182
making of modern civilization D 182
make up modern civilization D 182
make the system inoperative D 182
male or female respondents D 181
mask of sturdy contentment D 180
made to modern civilization D 180
made the initial presentation D 180
made by various astronomers D 180
made as seemed practicable D 180
mark of patent portmanteaus D 176
make the latter subservient D 174
make it highly undesirable D 174
made by female parishioners D 174
make the special arrangements D 172
made by survey respondents D 172
made at police headquarters D 172
make the farmer independent D 170
made up pretty exclusively D 170
made the actual arrangements D 170
made in school organization D 170
male or female perspective D 169
made to reduce drastically D 166
male in sexual intercourse D 164
make the latter subordinate D 164
made to suffer indignities D 162
made by french protestants D 162
made of richly embroidered D 161
make the sexual relationship D 158
make the scheme practicable D 158
make the masses politically D 158
make the crucial distinctions D 158
make the clergy independent D 158
made to reduce inventories D 158
made of sodium bicarbonate D 158
make up missed assignments D 156
make the ground practicable D 156
make the formal arrangements D 156
made the flight arrangements D 156
made my little arrangements D 156
made in various publications D 156
mass of minute crystalline D 154
make the tenant responsible D 154
made of public expenditure D 154
making the entire organization D 152
make the ladies comfortable D 152
made of existing inclinations D 152
male or female protagonist D 151
mass of profit appropriated D 150
mass of peasant cultivators D 150
mass of highly inflammable D 150
make the actual arrangements D 150
made to further investigate D 150
made the papacy subservient D 150
mass of oxygen transferred D 148
mark of cordial satisfaction D 148
making the formal presentation D 148
made the wisest preparations D 148
made at higher frequencies D 148
mass of proper consistence D 146
make the nearest practicable D 146
make the client responsible D 146
made in various communities D 146
make my travel arrangements D 144
made an initial presentation D 144
making the initial temperature D 142
make the seller responsible D 142
make the animal comfortable D 142
male or female interviewer D 141
making the client comfortable D 140
making it almost impregnable D 140
make my further arrangements D 140
make an office appointment D 140
made it almost prohibitive D 140
male or female counterpart D 139
making the annual appropriation D 138
make no further arrangements D 138
made to obtain statistical D 138
made it almost unmanageable D 136
made in future presupposes D 136
made by direct chlorination D 136
male or female prostitution D 134
making the various preparations D 134
making the judges irremovable D 134
make the proper requisitions D 134
make the dinner reservations D 134
made the latter responsible D 134
male or female chromosomes D 132
make it almost superfluous D 132
made of highly inflammable D 132
make the teacher responsible D 130
make the system susceptible D 130
make the proper disclosures D 130
make the person susceptible D 130
made to measure objectively D 130
made no proper preparations D 130
made no formal arrangements D 130
made my family comfortable D 130
made an entire consecration D 130
mass of waters attentively D 128
make the peasant susceptible D 128
make the initial installation D 128
make the dollar convertible D 128
make the church responsible D 128
made by another distinguished D 128
making the initial presentation D 126
made to obtain photographs D 126
mating in female chimpanzees D 125
making the police accountable D 125
make the measure distasteful D 124
made us pretty comfortable D 124
made the parish responsible D 124
made the crucial breakthroughs D 124
make the proper presentation D 122
make the proper experiments D 122
made the initial investments D 122
made of recent publications D 122
made in earlier publications D 120
made an almost impregnable D 120
making the person susceptible D 118
make the proper investments D 118
make do without electricity D 118
made to secure congressional D 118
made it fairly comfortable D 118
made by various disciplines D 118
made by public subscription D 118
mass of carbon compartment D 116
make the former predominate D 114
make an equity contribution D 114
made to revive agriculture D 114
made the states responsible D 114
made the master responsible D 114
made of animal experiments D 114
made in recent scholarship D 114
made in existing arrangements D 114
made an annual subscription D 114
mark the twelfth anniversary D 112
making is purely legislative D 112
make the former superfluous D 112
made to ensure coordination D 112
made the proper distinctions D 112
made the church subordinate D 112
made the actual presentation D 112
made by police departments D 112
male is further distinguished D 110
making the seller responsible D 110
making the person comfortable D 110
make the finest distinctions D 110
make the entire presentation D 110
make my proper prostrations D 110
made no proper arrangements D 110
made an inward prohibitory D 110
make the public responsible D 108
make the owners responsible D 108
make the annual appropriation D 108
making the person responsible D 106
make the system impractical D 106
make the school responsible D 106
make the needed distinctions D 106
make the building comfortable D 106
make no further acquisitions D 106
make an annual statistical D 106
make an annual presentation D 106
made it almost exclusively D 106
made in public enterprises D 106
male or female practitioner D 105
maze of narrow cobblestone D 104
mass of purely descriptive D 104
make the various arrangements D 104
make the nicest distinctions D 104
make it highly impractical D 104
make an initial appropriation D 104
made no further explorations D 104
made by county magistrates D 104
mass of solute transferred D 102
make it something mysteriously D 102
made to various departments D 102
made the police responsible D 102
made in cotton manufacturing D 102
mark as merely descriptive D 101
making the tenant responsible D 100
make the system practicable D 100
make the initial notification D 100
make the formal organization D 100
make no further distinctions D 100
made to metric measurements D 100
made the feudal aristocracy D 100
made no direct experiments D 100
made by various distinguished D 100
mass of french ultramarine D 98
make the proper assignments D 98
made to secure ratification D 98
made no gender distinctions D 98
make the states responsible D 96
made of velvet embroidered D 96
made no special appropriation D 96
made by modern scholarship D 96
made by modern civilization D 96
made by modern astronomers D 96
made by french astronomers D 96
make it fairly comfortable D 94
made the clergy independent D 94
made the bishop responsible D 94
made of rubber impregnated D 94
made no public declarations D 94
made in partial fulfillment D 94
made by various communities D 94
made at various conferences D 94
making the latter subordinate D 92
making the farmer independent D 92
make the shipping arrangements D 92
make no special preparations D 92
made to obtain legislative D 92
made the solemn protestation D 92
made the rivers subservient D 92
made the nicest calculations D 92
made the needed arrangements D 92
made by french missionaries D 92
mass of ingenious experiments D 90
making the masses politically D 90
make the public enterprises D 90
make the fewest adjustments D 90
made to public enterprises D 90
made to measure temperature D 90
made of fabric impregnated D 90
maze of secret passageways D 88
making the church subordinate D 88
make no farther observations D 88
make it highly commendable D 88
made to verify empirically D 88
made the police distrustful D 88
made the market subordinate D 88
made in various disciplines D 88
made by various manufacturing D 88
made by applying statistical D 88
mass of stringed instruments D 86
make the system commercially D 86
make the police commissioner D 86
make the french respectable D 86
make it highly inadvisable D 86
make it almost unendurable D 86
made to select respondents D 86
made of special lightweight D 86
made it highly undesirable D 86
made by earlier astronomers D 86
mark the opinion entertained D 84
maps the spatial distribution D 84
making the victim responsible D 84
making the system insensitive D 84
making the initial investments D 84
making the actual presentation D 84
make the visual presentation D 84
make the initial preparations D 84
make the highly questionable D 84
make the family responsible D 84
make the existing arrangements D 84
make the desire overmastering D 84
made to various publications D 84
made the little preparations D 84
made the former subservient D 84
made by electing legislators D 84
made at learned conferences D 84
made at higher temperature D 84
mark the golden anniversary D 82
making the little preparations D 82
make the agency responsible D 82
made to recent publications D 82
made to ensure continuation D 82
made no mental reservations D 82
mark the favour entertained D 80
mark of higher civilization D 80
maps is called cartography D 80
make the changes practicable D 80
make the ballet concentrate D 80
make by coloring photographs D 80
make an annual expenditure D 80
made to effect settlements D 80
made the schism irremediable D 80
made the cordial intercourse D 80
making in weapons development D 63
making in public enterprises D 58
making of modern malnutrition D 56
making of special assessments D 41
make the church subordinate D 41
made no farther improvement D 41
