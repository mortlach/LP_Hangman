game theory and related M 2655
gave meaning and purpose M 1664
gave food and shelter M 1128
gain from his quarter M 843
game theory and decision M 824
gave over all thoughts M 818
game theory for applied M 766
gale from that quarter M 648
gain from this process M 616
gave great and general M 582
gain some new insight M 562
game plan for success M 548
gain from this chapter M 524
gain their own freedom M 508
gave away his position M 490
gain their own selfish M 478
gave away his fortune M 429
gave what was perhaps M 428
game theory and rational M 428
gain time for preparing M 377
gave form and content M 365
gave over one hundred M 310
gave life and variety M 308
gave over two hundred M 298
game plan for achieving M 298
gain time and prepare M 297
gave only one example M 296
gave their own account M 292
gate open and stepped M 284
gain food and shelter M 278
gain time for general M 266
gain wealth and position M 263
gave away our position M 258
gave clear and precise M 256
gain time and prevent M 249
gain from its success M 248
gave nothing but pleasure M 246
gain from any increase M 236
gave their own opinions M 228
gave away two hundred M 228
gain from this project M 228
gain some real insight M 222
gain from all sources M 221
gave hope and comfort M 220
gave help and support M 218
game theory can provide M 217
gain from his efforts M 212
game there are certain M 209
gave away one hundred M 208
gave over his command M 206
gave away his thoughts M 206
gave only one lecture M 202
gate when they arrived M 200
gave upon that occasion M 196
gain love and respect M 191
gain from this program M 190
gave their own versions M 188
gave half his fortune M 182
gave form and purpose M 182
gain time for himself M 182
gain their own private M 182
gain from its subject M 179
gave away its position M 178
gain from his illness M 176
gain from this exchange M 172
gave over his pursuit M 170
gave life and freedom M 168
gain nothing for himself M 167
gave away ten thousand M 166
gave meaning and content M 162
gave away her position M 160
gave upon this occasion M 156
gave time for admiral M 156
gave only one hundred M 156
gave away his belongings M 154
gave very few details M 152
gain from our victory M 152
gave will his private M 150
gave birth and impulse M 146
gave upon this subject M 144
gain nothing but trouble M 143
gain from this alliance M 142
gain time she repeated M 140
gain from his decision M 140
gave very low priority M 136
gave forth her increase M 136
game theory and systems M 136
gain from this research M 135
gave from his private M 134
gate have they brought M 134
game from his windows M 134
game theory may provide M 132
game room and library M 132
gain more for himself M 132
gave rise for concern M 130
gave nothing but trouble M 128
gave help and comfort M 128
gain from all passive M 128
gave meaning and dignity M 126
gave only weak support M 124
gave only one concert M 124
gave forth her oracles M 124
gave away his location M 124
gave their own answers M 122
gain from his friends M 122
gave time for general M 120
gave rise and primary M 120
gave away any secrets M 120
gain time and deceive M 119
gave birth and brought M 118
gave away her thoughts M 118
gave those who opposed M 116
gave their own peculiar M 116
gain what they desired M 116
gave more than fifteen M 114
gave away two thousand M 114
gain from this marriage M 113
gave what they alleged M 112
game during his college M 112
gain peace and balance M 110
game theory and utility M 109
game theory and general M 109
game will not succeed M 108
gave birth and freedom M 106
gave away his worldly M 106
game plan for victory M 106
game fish are usually M 106
gain more than anybody M 106
gain many new friends M 105
gave over his purpose M 104
gave over his attempt M 104
gave away one thousand M 104
game there are several M 104
gain when they receive M 103
gave their own persons M 102
gave forth his oracles M 102
gate open and entered M 102
gain wealth for himself M 101
gave full and careful M 100
gate into that country M 100
gave such long lessons M 98
gate when she arrived M 98
gate open and started M 98
game theory has focused M 98
gain from this position M 97
gave very low results M 94
gave hope that perhaps M 94
gate there was written M 94
gain time and perhaps M 94
gave more than nominal M 92
gave peace and comfort M 88
gave over our purpose M 88
gave depth and variety M 88
gain time was everything M 88
gave away his peculiar M 86
game plan for systems M 86
gain time for improving M 86
gain from its adoption M 85
gave when her friends M 84
gave meaning and promise M 84
gave among his friends M 84
gain time than because M 84
gain from its victory M 84
gave worth and dignity M 82
gave these men letters M 82
gave from its central M 82
gain time with tyranny M 82
gave away his private M 80
gate when you arrived M 80
game from his position M 80
gain some new emotions M 80
gave hope and promise M 57
gain from this journey M 56
gain from his position M 56
gave more than seventy M 52
game theory can explain M 50
gain help and support M 48
gain from this decision M 44
gain from that quarter M 42
gain from that process M 42
gain from this increase M 41
gain fame and fortune D 1815
gave only lip service D 1444
gave your men courage D 762
gave hope and courage D 454
gave many sad strokes D 420
gave unto her husband D 338
gaping gulf with letters D 333
gaze held her captive D 322
game theory and national D 312
gain roll off rapidly D 274
gaze upon her husband D 273
gain from her foreign D 272
gain more than offsets D 248
gaze from her husband D 234
gave tone and texture D 216
gave life and courage D 205
gaze upon his forlorn D 198
gazing upon and listening D 178
gain fame for himself D 177
gain from his surmise D 168
gazing into this pleasant D 164
gaze upon his beloved D 161
gave oral and written D 152
gale hast thou weathered D 152
gazing into his crystal D 150
gave from his pierced D 144
gain from her husband D 143
gazing upon his beloved D 142
gave mere lip service D 142
gave away his pistols D 140
gaping open and splitting D 139
gaze upon his features D 135
gave some lip service D 128
gaping into his stomach D 128
gain time for warlike D 128
gaze upon its beauties D 127
gave gall and vinegar D 126
gate grew two silvery D 124
gave clear and concise D 120
game theory and dynamic D 120
gazing into her crystal D 119
gaze into our crystal D 119
gaze made her nervous D 118
gait were not adapted D 118
gazing upon its thousand D 116
gazing upon him fixedly D 116
gaze upon that gallant D 116
gage when his passions D 116
gave heart and courage D 112
gave time and courage D 110
gave over being puzzled D 110
gasp when she realized D 110
gain time for tampering D 110
gave food and raiment D 108
gave even lip service D 108
game room with billiard D 108
gall from our tempers D 108
gaze when she glanced D 106
gain hope and courage D 105
gazing into her husband D 104
gash over his eyebrow D 104
gazing upon that ghastly D 100
gaze upon its wonders D 100
gash over his forehead D 100
game laws are enacted D 100
gaze away and focused D 98
gazing eyes are brought D 94
gave more than cursory D 94
gave booth his liberty D 92
gazing past and present D 90
gave birth and nurture D 90
gave birth and impetus D 88
gala shoes and stockings D 88
gazing upon this picture D 87
gaze upon her unaware D 86
gave wealth and raiment D 86
gate leading her palfrey D 86
gain time for procuring D 86
gazing from her chamber D 82
gazing from this country D 80
gaze away and glanced D 80
gape open and swallow D 80
gaze upon this company D 61
gain from this venture D 52
gaze upon this picture D 49
gaze upon that glorious D 48
gaze upon her features D 43
gaze into her husband D 42
gaze upon her beloved D 40
gain from its foreign D 40
