import restrictions imposed by M 1853
impose significant burdens on M 712
impose unnecessary burdens on M 654
impose unreasonable burdens on M 526
import restrictions imposed on M 352
imposing unnecessary burdens on M 312
import restrictions imposed in M 301
imposing unreasonable burdens on M 264
impose unreasonable demands on M 248
import restrictions against the M 225
import substitution process in M 204
import restrictions involving the M 198
import restrictions because of M 150
impose unnecessary suffering on M 142
impose significant demands on M 142
import substitution because of M 141
impose unjustified burdens on M 134
import substitution efforts in M 124
import substitution because the M 122
import substitution program in M 121
import restrictions applied by M 119
impose restrictions similar to M 116
impose restrictions against the M 106
import substitution pursued by M 90
import substitution effects of M 89
imposing unreasonable demands on M 88
import restrictions adopted by M 86
impose centralized control on M 80
import restrictions imposed to M 43
import significant numbers of M 40
import significant amounts of D 511
impose intolerable burdens on D 452
imposing intolerable burdens on D 212
impart undesirable flavors to D 211
impose intolerable strains on D 198
imposing restrictive measures on D 176
impose restrictive measures on D 158
imputing disgraceful conduct to D 146
impose prohibitive tariffs on D 146
immune suppressive effects of D 134
imposing intolerable strains on D 132
impost established affecting the D 122
imposing amercements according to D 122
immune legislative conduct if D 94
import commodities handled by D 86
